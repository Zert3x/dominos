#!/usr/bin/env bash
echo "Building"
export CGO_ENABLED=1
env GOOS=windows GOARCH=amd64 CC=x86_64-w64-mingw32-gcc go build -o bin/dominos64.exe -ldflags="-s -w -H windowsgui" dominoschecker/cmd/gui
env GOOS=windows GOARCH=386 CC=x86_64-w64-mingw32-gcc go build -o bin/dominos.exe -ldflags="-s -w -H windowsgui" dominoschecker/cmd/gui
env GOOS=linux GOARCH=amd64 go build -o bin/dominos -ldflags="-s -w" dominoschecker/cmd/gui
echo "Packing"
upx bin/dominos64.exe
upx bin/dominos.exe
upx bin/dominos