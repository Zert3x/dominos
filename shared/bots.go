package shared

import (
	"fmt"
	"sync"
	"time"
)

const (
	CHECKING = iota
	BANNED
	STOPPED
)

type Bot struct {
	Current *Combo
	Status int
}

type BotManager struct {
	Semaphore chan int
	Wg *sync.WaitGroup
}

func (bm *BotManager) Start() {
	for i:=0; i < BotCount; i++ {
		go CheckerLoop(i)
	}

	go func() {
		for ;Running; {
			a := CManager.GetTotalCombos()-CManager.GetComboCount(UNCHECKED)
			b := CManager.GetTotalCombos()
			if a > 0 {
				CheckProgress.SetValue(float64(a)/float64(b))
			}
			ComboCounter.SetText(fmt.Sprintf("Combos: %d/%d", a, b))
			ValidCombosCounter.SetText(fmt.Sprintf("Valid: %d", CManager.GetComboCount(VALID)))
			BadCombosCounter.SetText(fmt.Sprintf("Bad: %d", CManager.GetComboCount(BAD)))
			ProxyCounter.SetText(fmt.Sprintf("Proxies: %d", PManager.GetProxyCount()))
			time.Sleep(500 * time.Millisecond)
		}
	}()

	/*go func() {
		//for n := range HitChan
	}()*/
}