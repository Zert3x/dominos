package shared

import (
	"fyne.io/fyne"
	"fyne.io/fyne/widget"
	"os"
)

var (
	CManager = &ComboManager{}
	PManager = &ProxyManager{}
	BManager = &BotManager{}
	BotCount int = 15
	MainWindow fyne.Window
	Running = false

	HitChan chan *Combo

	CheckProgress *widget.ProgressBar
	HitsList *widget.Box
	ProxyCounter *widget.Label
	ComboCounter *widget.Label
	ValidCombosCounter *widget.Label
	BadCombosCounter *widget.Label
	OutFile *os.File
	Apiurl = "https://api.dominos.com/as/token.oauth2"
	powerFirst = "https://order.dominos.com/power/login"
	powerSecond = "https://order.dominos.com/power/customer/%s/loyalty"
)

type FirstRequestResp struct {
	AccessToken string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	TokenType string `json:"token_type"`
	ExpiresIn int `json:"expires_in"`
}

type SecondRequestResp struct {
	Addresses []interface{} `json:"Addresses"`
	Age13OrOlder        bool          `json:"Age13OrOlder"`
	AgreeToTermsOfUse   bool          `json:"AgreeToTermsOfUse"`
	AlternateExtension  string        `json:"AlternateExtension"`
	AlternatePhone      string        `json:"AlternatePhone"`
	AsOfTime            string        `json:"AsOfTime"`
	BirthDate           string        `json:"BirthDate"`
	CustomerID          string        `json:"CustomerID"`
	CustomerIdentifiers []interface{} `json:"CustomerIdentifiers"`
	Email               string        `json:"Email"`
	EmailOptIn          bool          `json:"EmailOptIn"`
	EmailOptInTime      string        `json:"EmailOptInTime"`
	Extension           string        `json:"Extension"`
	FirstName           string        `json:"FirstName"`
	Gender              string        `json:"Gender"`
	IPAddress           string        `json:"IPAddress"`
	LastLogin           string        `json:"LastLogin"`
	LastName            string        `json:"LastName"`
	Market              string        `json:"Market"`
	Phone               string        `json:"Phone"`
	PhonePrefix         string        `json:"PhonePrefix"`
	SmsOptIn            bool          `json:"SmsOptIn"`
	SmsOptInTime        string        `json:"SmsOptInTime"`
	SmsPhone            string        `json:"SmsPhone"`
	Status              int           `json:"Status"`
	TaxInformation      []interface{} `json:"TaxInformation"`
	URL        string `json:"URL"`
	UpdateTime string `json:"UpdateTime"`
}

type ThirdRequestResp struct {
	AccountStatus           string `json:"AccountStatus"`
	BasePointExpirationDate string `json:"BasePointExpirationDate"`
	CustomerID              string `json:"CustomerID"`
	EnrollDate              string `json:"EnrollDate"`
	LastActivityDate        string `json:"LastActivityDate"`
	LoyaltyCoupons          []struct {
		BaseCoupon    bool   `json:"BaseCoupon"`
		CouponCode    string `json:"CouponCode"`
		LimitPerOrder string `json:"LimitPerOrder"`
		PointValue    int    `json:"PointValue"`
	} `json:"LoyaltyCoupons"`
	PendingPointBalance int `json:"PendingPointBalance"`
	VestedPointBalance  int `json:"VestedPointBalance"`
}