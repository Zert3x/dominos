package screens

import (
	"dominoschecker/shared"
	"fmt"
	"fyne.io/fyne"
	"fyne.io/fyne/dialog"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
	"regexp"
	"strconv"
)

func Checker(a fyne.App) fyne.CanvasObject {
	shared.CheckProgress = widget.NewProgressBar()

	shared.HitsList = widget.NewVBox()
	startBtn := widget.NewButton("Start", nil)

	botCountEntry := widget.NewEntry()
	botCountEntry.SetText("15")
	botCountEntry.OnChanged = func(s string) {
		reg, err := regexp.Compile("[^0-9]+")
		if err != nil {
			return
		}
		processedStr := reg.ReplaceAllString(s, "")
		botCountEntry.SetText(processedStr)
		shared.BotCount, err = strconv.Atoi(processedStr)
	}

	shared.ComboCounter = widget.NewLabel(fmt.Sprintf("Combos: %d/%d", shared.CManager.GetTotalCombos()-shared.CManager.GetComboCount(shared.UNCHECKED) ,shared.CManager.GetTotalCombos()))
	shared.ValidCombosCounter = widget.NewLabel(fmt.Sprintf("Valid: %d", shared.CManager.GetComboCount(shared.VALID)))
	shared.BadCombosCounter = widget.NewLabel(fmt.Sprintf("Bad: %d", shared.CManager.GetComboCount(shared.BAD)))
	shared.ProxyCounter = widget.NewLabel(fmt.Sprintf("Proxies: %d", shared.PManager.GetProxyCount()))

	startBtn.OnTapped = func() {
		if shared.CManager.GetTotalCombos() == 0 || shared.PManager.GetProxyCount() == 0 {
			dialog.NewInformation("Empty list", "You must load both proxies and combos.", shared.MainWindow).Show()
			return
		}
		shared.Running = !shared.Running
		if shared.Running {
			startBtn.Text = "Stop"
			shared.BManager.Start()
		} else {
			startBtn.Text = "Start"
		}
	}

	return fyne.NewContainerWithLayout(layout.NewVBoxLayout(),
		widget.NewHBox(
			widget.NewVBox(
				shared.ComboCounter,
				shared.ValidCombosCounter,
				shared.BadCombosCounter,
				shared.ProxyCounter,
				widget.NewHBox(
					widget.NewLabel("Bots to use"),
					botCountEntry,
					),
				layout.NewSpacer(),
				),
			widget.NewVBox(
				widget.NewLabelWithStyle("Hits", fyne.TextAlignCenter, fyne.TextStyle{}),
				shared.HitsList,
				),
		),
		layout.NewSpacer(),
		widget.NewHBox(
			shared.CheckProgress,
			startBtn,
		),
	)

}